import argparse

import redis

def parseArguments():
    parser = argparse.ArgumentParser(
            usage = '%s [options] channel jsonfile' % __file__,
            formatter_class=argparse.RawDescriptionHelpFormatter,
            description=__doc__)
    parser.add_argument("--host", type=str, default="localhost", help="Redis server host")
    parser.add_argument("--port", type=int, default=6379, help="Redis server port")
    parser.add_argument("channel", type=str, help="PubSub service channel name")
    parser.add_argument("json", type=str, help="The json file to publish")
    return parser.parse_args()
    

def main():
    args = parseArguments()

    r = redis.StrictRedis(host=args.host, port=args.port, db=0)
    f = open(args.json, 'r')
    r.publish(args.channel, f.read())

if __name__ == '__main__':
    main()
